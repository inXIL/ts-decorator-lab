interface MagVulnerableComponent {
    takeMagicDamage(damage: number): void;
}

// might add `n_shields` to make shield pop more robust
class WeakHealthEssence implements MagVulnerableComponent {
    private health = 600; // can get below 0
    takeMagicDamage(damage: number) {
        const health_after = this.health - damage;
        console.log(`| ${Math.floor(this.health)}->${Math.floor(health_after)}`);
        this.health = health_after;
    }
}

abstract class MagicShieldDecorator implements MagVulnerableComponent {
    protected magVulnerable: MagVulnerableComponent;
    constructor(shield: MagVulnerableComponent) {
        this.magVulnerable = shield;
    }
    abstract takeMagicDamage(damage: number): void;
    getInner() {
        return this.magVulnerable;
    }
}


// probably not the best idea, cause it's not going to be removed once it's destroyed inside decorator stack
class ConstantMagicShield extends MagicShieldDecorator {
    private capacity: number;
    constructor(magVulnerable: MagVulnerableComponent, capacity: number) {
        super(magVulnerable);
        if (capacity < 0) {
            throw new Error("Negative ConstantMagicShield");
        }
        this.capacity = capacity;
    }
    takeMagicDamage(damage: number) {
        const damage_reduction = Math.min(this.capacity, damage);
        this.capacity -= damage_reduction;
        const damage_after = damage - damage_reduction;
        process.stdout.write(`C${Math.floor(damage)}->${Math.floor(damage_after)} `);
        this.magVulnerable.takeMagicDamage(damage_after);
    }
}

class MResistMagicShield extends MagicShieldDecorator {
    private readonly resistance: number;
    constructor(magVulnerable: MagVulnerableComponent, resistance: number) {
        super(magVulnerable);
        if (resistance < 0 || 1 < resistance) {
            throw new Error("out of range MResistMagicShield");
        }
        this.resistance = resistance;
    }
    takeMagicDamage(damage: number) {
        const damage_after = (1 - this.resistance) * damage;
        process.stdout.write(`R${Math.floor(damage)}->${Math.floor(damage_after)} `);
        this.magVulnerable.takeMagicDamage(damage_after);
    }
}


// Client code below:

class SkeletonSoldier {
    private health_essence: MagVulnerableComponent = new WeakHealthEssence();
    takeMagicDamage(damage: number) {
        this.health_essence.takeMagicDamage(damage);
    }
    stackConstantMagicShield(capacity: number) {
        this.health_essence = new ConstantMagicShield(this.health_essence, capacity);
    }
    stackMResistMagicShield(resistance: number) {
        this.health_essence = new MResistMagicShield(this.health_essence, resistance);
    }
    popMagicShield() {
        this.health_essence = (this.health_essence as MagicShieldDecorator).getInner();
        console.log("pop");
    }
}

{
    console.log("not very useful, static control flow and data:");
    let skeleton = new SkeletonSoldier();

    skeleton.takeMagicDamage(50);

    skeleton.stackConstantMagicShield(200);
    skeleton.takeMagicDamage(50);
    skeleton.takeMagicDamage(200);

    skeleton.stackMResistMagicShield(0.1);
    skeleton.takeMagicDamage(100);
    skeleton.popMagicShield();
    skeleton.takeMagicDamage(100);

    skeleton.stackMResistMagicShield(0.1);
    skeleton.stackConstantMagicShield(200);
    skeleton.stackMResistMagicShield(0.1);
    skeleton.stackMResistMagicShield(0.1);
    skeleton.stackMResistMagicShield(0.1);
    skeleton.takeMagicDamage(700);
}

{
    console.log("\nDYNAMIC:");
    let skeleton = new SkeletonSoldier();
    let n_shields = 0;
    for(let i = 0; i < 30; ++i) {
        function randint(min, max) {
            return Math.floor(min + Math.random()*(max + 1 - min));
        }
        const act_n = randint(0, 6);
        switch(act_n) {
            case 0: {
                skeleton.takeMagicDamage(Math.random() * 1000);
            } break;
            case 1: case 2: {
                skeleton.stackConstantMagicShield(Math.random() * 300);
                ++n_shields;
            } break;
            case 3: case 4: case 5: {
                skeleton.stackMResistMagicShield(Math.random() * 0.5);
                ++n_shields;
            } break;
            case 6: {
                if (n_shields > 0) {
                    skeleton.popMagicShield();
                    --n_shields;
                }
            } break;
        }
    }
}
